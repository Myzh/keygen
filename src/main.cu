#include "main.cuh"


// implementation of CUDA functions

__global__ void generatePartialKeystream(uint64_t iterations, uint16_t blockSize, uint8_t key, BinaryShiftMatrix matrix, uint8_t *keystream) {

  const uint32_t startIndex = (blockIdx.x * CUDA_THREADS + threadIdx.x) * (blockSize + 1);
  const uint32_t increment = gridDim.x * CUDA_THREADS * (blockSize + 1);

  uint8_t shiftRegister = keystream[startIndex];

  // generate the keystream
  for (uint64_t i = startIndex; i < iterations; i += increment) {

    // do not calculate on the first iteration
    if (i != startIndex) {

      // apply the matrix to the register
      shiftRegister = matrix * shiftRegister;

      // add the character to the keystream
      keystream[i] = shiftRegister;
    }
    
    for (uint16_t j = 0; j < blockSize; j++) {

      // calculate the rest of the block
      keystream[i + j + 1] = lfsrCalculate(keystream[i + j], key);
    }
  }

}


// implementation of helper functions

__host__ __device__ inline uint8_t lfsrCalculate(uint8_t input, uint8_t key) {

  // run the feedback loop the key length times
  for (uint8_t j = 0; j < 8; j++) {

    uint8_t feedback = input & key;
    bool nextBit = 0;

    // shift the register by one bit
    input <<= 1;

    // calculate the next bit
    for (uint8_t k = 0; k < 8; k++) {

      nextBit = nextBit ^ (bool) (feedback & (1 << k));
    }

    // append the next bit to the register
    input |= nextBit;
  }

  return input;
}


// implementation of member functions


// BinaryShiftMatrix

BinaryShiftMatrix::BinaryShiftMatrix() {

  // create a null matrix
  memset(&values, 0, 8);
}

BinaryShiftMatrix::BinaryShiftMatrix(uint8_t key) {
  
  for (uint8_t i = 0; i <= 6; i++) {
  
    // set shift matrix parts
    values[i] = (1 << (6 - i));
  }

  // copy the key as the last row of the matrix
  values[7] = key;
}

__host__ __device__ inline uint8_t BinaryShiftMatrix::operator*(uint8_t &vector) {

  uint8_t ret = 0;

  for (uint8_t i = 0; i <= 7; i++) {
  
    // add the vector on top of the matrix line
    uint8_t line = values[i] & vector;

    bool current = false;

    // calculate the binary checksum as the value of the bit of the return value
    for (uint8_t j = 0; j <= 7; j++) {

      current ^= (bool) (line & (1 << j));
    }

    ret |= current << (7 - i);
  }

  return ret;
}

inline BinaryShiftMatrix BinaryShiftMatrix::operator*(BinaryShiftMatrix &matrix) {

  BinaryShiftMatrix ret;

  for (uint8_t i = 0; i <= 7; i++) {

    uint8_t line = 0;

    for (uint8_t j = 0; j <= 7; j++) {

      bool current = false;

      for (uint8_t k = 0; k <= 7; k++) {

        // multiply the own matrix rows on top of the input matrix collumns and add the result to the current cell
        current ^= (values[i] & (1 << (7 - k))) && (matrix.values[k] & (1 << j));
      }

      // add the current cell to the line
      line |= current << j;
    }
  
    // add the completed line to the return value
    ret.values[i] = line;
  }

  return ret;
}

inline BinaryShiftMatrix BinaryShiftMatrix::operator^(const uint32_t &power) {

  BinaryShiftMatrix ret = *this;

  if (power & (1U << 31)) {
    throw std::out_of_range("Power is out of range");
  }

  uint32_t i;
  for (i = 2; i <= power; i <<= 1) {
  
    // multiply this matrix over and over again
    ret = ret * ret;
  }

  // shift back again
  i >>= 1;

  if ((power - i) != 0) {

    // calculate the rest recursively
    BinaryShiftMatrix rest = ((*this) ^ (power - i));
    ret = ret * rest;
  }

  return ret;
}


int32_t main() {

  int8_t mode;

  // key length = 8
  uint8_t key;
  uint64_t keystreamLength;
  uint8_t *keystream;

  // get the user input
  {

    int32_t scanResult;

    do {

      std::cout << "Enter a non-zero key [hex, 8-bits]: ";
      scanResult = scanf("%hhx", &key);

    } while ((key == 0) || (scanResult != 1));

    std::cout << "Enter the length of the generated keystream: ";
    std::cin >> keystreamLength;

    std::cout << "Use a parallel computing? [y/N]: ";
    std::cin >> mode;
  }

  // important for accurate performance measuring
  std::ios_base::sync_with_stdio(false);
  struct timeval start, stop;

  // decide on the mode
  if (mode == 'y' || mode == 'Y') {

    // use parallelization
    uint16_t coreCount;
    uint16_t blockSize;

    // get more user input
    {

      do {

        std::cout << "Enter the amount of parallel computing units: ";
        std::cin >> coreCount;

      } while (coreCount == 0);
    }

    std::cout << "Enter the block size: ";
    std::cin >> blockSize;

    gettimeofday(&start, NULL);

    // allocate memory in multiples of the core count and block size
    uint64_t minimalKeystreamLength;
    uint64_t keystreamLengthMemory;

    minimalKeystreamLength = CUDA_THREADS * coreCount * (blockSize + 1);
    keystreamLengthMemory = ((keystreamLength / minimalKeystreamLength) + 1) * minimalKeystreamLength;
    keystream = new uint8_t[keystreamLengthMemory];

    // generate the matrix
    BinaryShiftMatrix singleStepMatrix(key);
    BinaryShiftMatrix initialStepMatrix = singleStepMatrix ^ (8 * (blockSize + 1));
    BinaryShiftMatrix bulkStepMatrix = initialStepMatrix ^ (CUDA_THREADS * coreCount);

    // calculate the first value
    keystream[0] = lfsrCalculate(~key, key);

    // generate the first part of the keystream
    for (uint32_t i = 1; i < (CUDA_THREADS * coreCount); i++) {

      // add the character to the keystream
      keystream[(blockSize + 1) * i] = initialStepMatrix * keystream[(blockSize + 1) * (i - 1)];
    }

    // initialize the device memory
    uint8_t *deviceKeystream;
    cudaMalloc(&deviceKeystream, keystreamLengthMemory);
    cudaMemcpy(deviceKeystream, keystream, minimalKeystreamLength, cudaMemcpyHostToDevice);

    // start the cores
    generatePartialKeystream<<<coreCount, CUDA_THREADS>>>(keystreamLengthMemory, blockSize, key, bulkStepMatrix, deviceKeystream);

    cudaDeviceSynchronize();

    // copy the results back and free the device memory
    cudaMemcpy(keystream, deviceKeystream, keystreamLengthMemory, cudaMemcpyDeviceToHost);
    cudaFree(deviceKeystream);

  } else {

    gettimeofday(&start, NULL);

    // do not use parallezation
    keystream = new uint8_t[keystreamLength];

    // calculate the first value
    keystream[0] = lfsrCalculate(~key, key);

    // generate the keystream
    for (uint64_t i = 1; i < keystreamLength; i++) {

      // add the character to the keystream
      keystream[i] = lfsrCalculate(keystream[i - 1], key);
    }

  }

  gettimeofday(&stop, NULL);

  std::ios_base::sync_with_stdio(true); 

  char printKeystream;

  // get more user input
  {

    std::cout << "Print the generated keystream: ";
    std::cin >> printKeystream;
  }

  // decide wether to print the output
  if (printKeystream == 'y' || printKeystream == 'Y') {

    std::cout << "Keystream: " << std::endl << std::flush;

    // output the keystream in hexadecimal
    for (uint32_t i = 0; i < keystreamLength; i++) {

      std::cout << std::setw(2) << std::setfill('0') << std::hex << unsigned(keystream[i]) << " ";
    }

    std::cout << std::endl << std::flush;

  }

  // output the execution time
  {

    struct timeval diff;
    timersub(&stop, &start, &diff);

    std::cout << "Execution time: " << std::dec << diff.tv_sec << "." << std::setw(5) << std::setfill('0') << std::dec << diff.tv_usec << "s" << std::endl; 
  }

  delete[] keystream;

  return 0;
}
