#include <iostream>
#include <iomanip>
#include <cmath>
#include <stdio.h>
#include <inttypes.h>
#include <sys/time.h>


// defines

#define CUDA_THREADS 128


// forward definition of classes

class BinaryShiftMatrix;


// forward delcaration of CUDA functions

__global__ void generatePartialKeystream(uint64_t iterations, uint32_t blockSize, uint8_t key, BinaryShiftMatrix matrix, uint8_t *keystream);


// forward delcaration of helper functions

__host__ __device__ inline uint8_t lfsrCalculate(uint8_t input, uint8_t key);


// classes

class BinaryShiftMatrix {

public:
  BinaryShiftMatrix();
  BinaryShiftMatrix(uint8_t key);

  __host__ __device__ inline uint8_t operator*(uint8_t &vector);
  inline BinaryShiftMatrix operator*(BinaryShiftMatrix &matrix);
  inline BinaryShiftMatrix operator^(const uint32_t &power);

protected:
  uint8_t values[8];

};
