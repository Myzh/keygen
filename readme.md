# Optimization of Conventional Stream Ciphers report and CUDA demonstrator

## The report

1. Install TeX Live (full package).

2. Compile the report.
```
latex streamciphers.tex
bibtex streamciphers.aux
makeglossaries streamciphers
pdflatex streamciphers.tex mupdf
```

## The CUDA demonstrator for the parallelization of LFSRs

CMake 3.18 and CUDA is required.

1. Setup the build emvironment
```
mkdir build
cd build
cmake ..
```

2. Build the project
```
make keygen
```

3. Run the program
```
../bin/keygen
```
