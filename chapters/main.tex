\subsection{Description of Stream Ciphers} 

  \begin{equation}
    \label{eq:cem}
    c = e_K(m) = k \oplus m
  \end{equation}

  \begin{equation}
    \label{eq:mdc}
    m = d_K(c) = k \oplus c
  \end{equation}

  A generic stream cipher consists of a keystream generator and an encryption $e$ or decryption $d$ function, depending on the party.
  An input key or seed $K$ and the plaintext message $m$ will be taken as inputs to the cipher as visualized in Figure \ref{fig:cipherArch}.
  The keystream generator will generate a pseudo-random number sequence from the seed called the keystream.
  Once the keystream has been generated, it can be applied to the plaintext using encryption function.
  In most cases, this will be just a bitwise XOR-operation of the keystream and the message \eqref{eq:cem}, as it ensures that both the encryption and decryption process is identical (\ie the cipher is symmetric) \eqref{eq:mdc}.
  This is due to the bijective property of the XOR-function given that the same key is used.
  After the operation has been performed, the ciphertext $c$ can be sent to another party. \cite[~179]{smart2016}
  The operation will be performed independently on every bit or byte depending on the cipher used.

  \begin{figure}[h]
    \centering
    
    \begin{tikzpicture}[node distance = 30pt and 120pt]

      % nodes
      \node(partyA)[empty]{};
      \node(encr)[operation, right = of partyA, xshift=-90pt]{encryption};
      \node(keystreamEncr)[operation, below = of encr]{keystream generator};
      \node(keyEncr)[void, below = of keystreamEncr]{};
      \node(decr)[operation, right = of encr]{decryption};
      \node(keystreamDecr)[operation, below = of decr]{keystream generator};
      \node(keyDecr)[void, below = of keystreamDecr]{};
      \node(partyB)[empty, right = of decr, xshift=-90pt]{};

      % arrows
      \draw[arrow] (partyA) -- node[anchor = south, xshift=-20pt]{plaintext}(encr);
      \draw[arrow] (keystreamEncr) -- node[]{}(encr);
      \draw[arrow] (keyEncr) -- node[anchor = east]{seed}(keystreamEncr);
      \draw[arrow] (encr) -- node[anchor = south]{ciphertext}(decr);
      \draw[arrow] (keystreamDecr) -- node[]{}(decr);
      \draw[arrow] (keyDecr) -- node[anchor = west]{seed}(keystreamDecr);
      \draw[arrow] (decr) -- node[anchor = south, xshift = 20pt]{plaintext}(partyB);

    \end{tikzpicture}

    \caption{Basic architecture of a stream cipher}
    \label{fig:cipherArch}

  \end{figure}

  \subsubsection{Properties of the keystream generator}

    \begin{equation}
      \label{eq:keygenRec}
      k_i = g(k_{i - 1}) \qquad k_0 = K
    \end{equation}

    \begin{equation}
      \label{eq:keygenIter}
      k_i = g_K(i - 1)
    \end{equation}

    The basic idea behind a keystream generator $g$ is to transform a short initial key or seed $K$ that is easy to store and distribute into a long series of keys $k$.
    Keystream generators can either work recursively (\ie they generate the next state from information about the previous state) \eqref{eq:keygenRec} or be defined as a closed-form expression (\ie they generate any given state from its index $i$) \eqref{eq:keygenIter}.
    To avoid effective plaintext attacks, the keystream generator should be a strong deterministic \gls{rng}.
    There are certain cryptographic properties the generator should possess to ensure adequate randomness.
    Most importantly these include an equal distribution of values and a long period and non-linearity, among other properties. \cite[~25]{wuFeng2016}
    
  \subsubsection{Usage of \glspl{lfsr} as a keystream generator}

    A very popular technique of implementing a keystream generator is the usage of \glspl{lfsr}.
    \Glspl{lfsr} are, as the name implies, shift registers with a size $n$ that do not only store bits, but on a shift operation also generate the value of the \gls{lsb} with a linear function of its own values.
    This can be achieved by combining a collection of its values with XOR-gates as is shown in Figure \ref{fig:lfsrArch}.
    It is common practice to use static collections in a cipher (\ie not making it part of the key).
    If we assume the collection to be defined as a vector $\vec{a} \in \{0, 1\}^n$ and the current values as a vector $\vec{x}_i \in \{0, 1\}^n$, we can calculate the value of the \gls{lsb} of the next iteration $x_{(i + 1), 0}$ by simply multiplying the two vectors.
    
    \begin{equation}
      \label{eq:keygenRec}
      \vec{x}_{(i + 1), 0} \equiv \vec{a} \cdot \vec{x}_i \equiv \sum\limits_{j = 0}^{n - 1} a_j \cdot x_{i, j} \pmod{2}
    \end{equation}

    \begin{figure}[h]
      \centering
      
      \begin{tikzpicture}[node distance = 30pt]
  
        % nodes
        \node(stateA)[state]{$x_0$};
        \node(operationA)[empty, below = of stateA]{};
        \node(stateB)[state, right = of stateA]{$x_1$};
        \node(operationB)[operation, below = of stateB]{$\oplus$};
        \node(stateC)[state, right = of stateB]{$x_2$};
        \node(operationC)[empty, below = of stateC]{};
        \node(stateD)[state, right = of stateC]{$\ldots$};
        \node(operationD)[empty, below = of stateD]{};
        \node(stateE)[state, right = of stateD]{$x_{n - 1}$};
        \node(operationE)[operation, below = of stateE]{$\oplus$};
        \node(stateF)[empty, right = of stateE]{};
  
        % arrows and lines
        \draw[arrow] (stateA) -- node[]{}(stateB);
        \draw[arrow] (stateB) -- node[]{}(stateC);
        \draw[arrow] (stateC) -- node[]{}(stateD);
        \draw[arrow] (stateD) -- node[]{}(stateE);
        \draw[arrow] (stateE) -- node[]{}(stateF);
        \draw[arrow] (stateB) -- node[]{}(operationB);
        \draw[arrow] (stateE) -- node[]{}(operationE);
        \draw[arrow] (operationE) -- node[]{}(operationB);
        \draw[line] (operationB) -- node[]{}(operationA.center);
        \draw[arrow] (operationA.center) -- node[]{}(stateA);
  
      \end{tikzpicture}
  
      \caption{Architecture of an \gls{lfsr}}
      \label{fig:lfsrArch}
  
    \end{figure}

    \Glspl{lfsr} are not very secure on their own.
    This is due to them violating fundamental concepts of good \glspl{rng}.
    Most notably, they produce self-repeating sequences with every value appearing at most once in a cycle.
    This is an intrinsic property of \glspl{lfsr} because once the initial value is reached again the cycle will inevitably repeat.
    We can therefore also think of \glspl{lfsr} as circular finite-state machines.
    As a result, it is rather easy for an attacker to compute keystream values from already known parts of the keystream. \cite{rfc4086}
    The period of the generated sequence $p$ varies between collection vectors $a$ but is always smaller than $2^n - 1$.
    This is the number of possible values that can be represented by the $n$ bits of the register excluding $0$ which forms its own infnite loop.
    We can combine multiple \glspl{lfsr} together to vastly increase the period and allow for repeating values in a cycle.
    In order to improve security the combination function should be non-linear yet still provide an equal distribution of values. \cite[26]{wuFeng2016}

  \subsubsection{Performance analysis of \glspl{lfsr}}

    The classic implementation of \glspl{lfsr} is strictly recursive.
    We can assume the time required to generate the next number in the sequence $t_{\gls{lfsr}, iter}$ to be constant.
    Therefore it is possible to derive that calculating the $m$-th number in the sequence has the computational complexity of $\mathcal{O}(m)$ (\ie linear complexity).
    The amount of time $t_{\gls{lfsr}, total}$ required to compute a sequence with the length $s$ can be calculated with the following formula. \eqref{eq:lfsrTime}

    \begin{equation}
      \label{eq:lfsrTime}
      t_{\gls{lfsr}, total} = t_{\gls{lfsr}, iter} \cdot s
    \end{equation}

\subsection{Matrix-based implementation of \glspl{lfsr}}

  \begin{equation}
    \label{eq:lfsrMatrix}
    M = \left[\begin{array}{cccccc} 0 & 1 & 0 & 0 & \cdots & 0 \\ 0 & 0 & 1 & 0 & \cdots & 0 \\ 0 & 0 & 0 & 1 & \cdots & 0 \\ \vdots & \vdots & \vdots & \vdots & \ddots & \vdots \\ 0 & 0 & 0 & 0 & \cdots & 1 \\ a_{n - 1} & a_{n - 2} & a_{n - 3} & a_{n - 4} & \cdots & a_0 \end{array}\right]
  \end{equation}

  \begin{equation}
    \label{eq:lfsrMatrixOperation}
    \vec{x}_{i + 1} \equiv M \cdot \vec{x}_i \pmod{2} \Longrightarrow \vec{x}_{i + m} \equiv M^m \cdot \vec{x}_i \pmod{2}
  \end{equation}

  To reduce the complexity we can represent an \gls{lfsr}-cylce as a matrix operation.
  The shift operation is represented by a shifted unit matrix, while the XOR summation for the \gls{lsb} is essentially the transposed collection vector $a$.
  These two operations combined give us the transformation matrix $M \in \{0, 1\}_{n \times n}$. \eqref{eq:lfsrMatrix}
  If we want to calculate the next iteration of values inside the register $x$, we can simply multiply it by $M$. \eqref{eq:lfsrMatrixOperation}
  As we now have an algebraic way of describing an \gls{lfsr} it is possible to raise $M$ to the $m$-th power in order to calculate the $m$-th element in the sequence.
  If $a_{n - 1} \neq 0$ then $M$ is diagonalizable (\ie $a_{n - 1} \neq 0$) and $M^m$ has constant complexity in regards to $m$.
  Otherwise, $M^m$ can still be computed in $\mathcal{O}(\log m)$, which is still good enough for most applications.
  Given the value of $\vec{x}_0$ (\ie the key), we are now able to efficiently calculate the value of a given index $i$.
  
  In practice, this matrix-based calculation is not very useful as we are interested in a continuous stream of values.
  For small indeces, the approach is actually a lot slower, as the initial calculation of $M^m$ can still take up a considerable amount of time, even with a low complexity.
  Additionally, the multiplication of a matrix with a vector takes up a considerable amount of time on its own.
  This also holds true when working with binary matrices, for which specialized algorithms exist. \cite{ieee9088014}
  The benefit of the transformation matrix does not lie in a direct performance increase, but rather in the ability to parallelize the process of generating a keystream.
  To accomplish this parallelization, we need to first calculate an initial sequence of value vectors $\vec{x}$ with a length $l \in \mathbb{N}$ using traditional \glspl{lfsr}.
  $l$ needs to equal the number of parallel compute units (cores) being used. 
  We now calculate $M^m$ where $m = n \cdot l$ with $n$ being the length of $x$.
  Afterward we assign every core a unique index $\iota \in \{1 \dots l\}$ and a working vector $\vec{w_\iota}$ with the corresponding initial value $\vec{w}_{\iota, 0} = \vec{x}_{\iota \cdot n}$ from the pre-calculated sequence.
  Every core can now repeat computing $\vec{w}_{\iota, i + 1} = M^m \cdot \vec{w}_{\iota, i}$ in order to generate a subsequence of the \gls{lfsr}-output. \cite{acns2003}
  The entire sequence can be recombined from the subsequences generated by the cores.
  During each iteration $n$ bits of the keystream are generated per core.
  With real-time parallelization, it is therefore possible to calculate $m$ bits simultaneously.

  \begin{figure}[h]
    \centering
    
    \begin{tikzpicture}[node distance = 15pt]

      % nodes
      \node(lfsr)[operation]{\gls{lfsr}};
      \node(aboveInit)[empty, below = of lfsr]{};
      \node(init)[state, below = of aboveInit]{$x_0 \dots x_{m - l}$};
      \node(BI)[empty, right = of lfsr]{};
      \node(AI)[empty, above = of BI]{};
      \node(CI)[empty, below = of BI]{};
      \node(A0)[operation, right = of AI]{$M^m$};
      \node(A1)[empty, right = of A0]{};
      \node(A2)[empty, right = of A1]{};
      \node(A3)[operation, right = of A2]{$M^m$};
      \node(A4)[empty, right = of A3]{};
      \node(AE)[empty, right = of A4]{};
      \node(B0)[empty, right = of BI]{};
      \node(B1)[operation, right = of B0]{$M^m$};
      \node(B2)[empty, right = of B1]{};
      \node(B3)[empty, right = of B2]{};
      \node(B4)[operation, right = of B3]{$M^m$};
      \node(BE)[empty, right = of B4]{};
      \node(C0)[empty, right = of CI]{};
      \node(C1)[empty, right = of C0]{};
      \node(C2)[operation, right = of C1]{$M^m$};
      \node(C3)[empty, right = of C2]{};
      \node(C4)[empty, right = of C3]{};
      \node(CE)[empty, right = of C4]{};
      \node(BI)[empty, right = of lfsr]{};
      \node(AI)[empty, above = of BI]{};
      \node(S0)[state, below = of C0]{$x_{1m + 0n}$};
      \node(S1)[state, below = of C1]{$x_{1m + 1n}$};
      \node(S2)[state, below = of C2]{$x_{1m + 2n}$};
      \node(S3)[state, below = of C3]{$x_{2m + 0n}$};
      \node(S4)[state, below = of C4]{$x_{2m + 1n}$};
      \node(SE)[empty, below = of CE]{};

      % arrows and lines
      \draw[arrow] (lfsr) -- node[]{}(init);
      \draw[line] (lfsr) -- node[]{}(BI.center);
      \draw[line] (BI.center) -- node[]{}(AI.center);
      \draw[line] (BI.center) -- node[]{}(CI.center);
      \draw[arrow] (AI.center) -- node[]{}(A0);
      \draw[arrow] (BI.center) -- node[]{}(B1);
      \draw[arrow] (CI.center) -- node[]{}(C2);
      \draw[arrow] (A0) -- node[]{}(A3);
      \draw[arrow] (B1) -- node[]{}(B4);
      \draw[arrow] (C2) -- node[]{}(CE.center);
      \draw[arrow] (A3) -- node[]{}(AE.center);
      \draw[arrow] (B4) -- node[]{}(BE.center);
      \draw[arrow, dashed] (A0) -- node[]{}(S0);
      \draw[arrow, dashed] (B1) -- node[]{}(S1);
      \draw[arrow, dashed] (C2) -- node[]{}(S2);
      \draw[arrow, dashed] (A3) -- node[]{}(S3);
      \draw[arrow, dashed] (B4) -- node[]{}(S4);

    \end{tikzpicture}

    \caption{Parallelization architecture of the \gls{lfsr}-output with $l = 3$}
    \label{fig:lfsrArch}

  \end{figure}

  \subsubsection{Performance analysis of the matrix-based implementation}

    As previously stated, the computation of a \gls{lfsr}-segment using a matrix is not very fast.
    We can estimate the amount of time $t_{matrix, total}$ required to compute a sequence with the length $s$ given number of parallel compute units $l$ by using the following formula. \eqref{eq:matrixTime}
    Notably $t_{matrix, total}$ increases in larger steps due to $\frac{s}{l}$ needing to be rounded up, as there are always $l$ values computed simultaneously.
    The time to compute a matrix operation $t_{matrix, iteration}$ is assumed to be equal for every core.
    Although the time to calculate the transformation matrix $t_{pre}$ is considered in the equation, for most applications this can be assumed to be $0$ as it would, with standardized collection vectors and core counts, be calculated through pre-processing.

    \begin{equation}
      \label{eq:matrixTime}
      t_{matrix, total} = t_{matrix, iter} \cdot \ceil*{\frac{s}{l}} + l \cdot t_{\gls{lfsr}, iteration} + t_{pre}
    \end{equation}

    \begin{equation}
      \label{eq:matrixRelationTime}
      \frac{t_{\gls{lfsr}, total}}{t_{matrix, total}} \approx l \cdot \frac{t_{\gls{lfsr}, iter}}{t_{matrix, iter}} \qquad s \gg l
    \end{equation}

    Combining both equations \eqref{eq:lfsrTime} and \eqref{eq:matrixTime} while ignoring irrelevant parts for large $s$ gives us \eqref{eq:matrixRelationTime} which can be used to estimate the speedup of the parallel algorithm for large sequences.
    As $t_{matrix, iteration}$ has a complexity of $\mathcal{O}(n^2)$ while $t_{\gls{lfsr}, iteration}$ only has a complexity of $\mathcal{O}(n)$ the parallel algorithm generally requires a large core count to be faster and therefore worth implementing.

\subsection{Combining \Glspl{lfsr} with matrix-based implementations}

  As already mentioned, the key advantage of using the matrix-based algorithm is the ability to efficiently calculate arbitrary values in the sequence.
  However, this index-based value generation is relatively slow compared to the sequential generation of \glspl{lfsr}.
  To further increase performance it is therefore necessary to combine both approaches.
  Instead of just generating singular values by repeatedly applying $M^m$ to a working vector, we could order the computation into coherent blocks of values to be generated by the cores.
  Here we calculate the first value in the block with the matrix and afterward generate the rest of the block of size $b$ recursively with an \gls{lfsr}.
  This combines the singular core performance of \glspl{lfsr} with the ability to parallelize the computation.

  \subsubsection{Performance analysis of the combined implementation}

    The bulk of operations are now done with the faster \glspl{lfsr} which geatly increases the performance. \eqref{eq:combinationTime}
    However, the computation of the initial sequence, as it is no longer coherent, now has to be performed by matrix multiplication.
    It should also be noted that due to $m_{combination} = n \cdot l \cdot b$ the time to calculate the transformation matrix $t_{pre}$ may increase slightly, depending on the calculation method used.
    Both these factors are insignificant though, as they are only a constant overhead and can be ignored if $s \gg l$.

    \begin{equation}
      \label{eq:combinationTime}
      t_{combination, total} = (t_{matrix, iter} + (b - 1) t_{\gls{lfsr}, iter}) \ceil*{\frac{s}{l \cdot b}} + l \cdot t_{matrix, iter} + t_{pre}
    \end{equation}

    \begin{equation}
      \label{eq:combinationRelationTime}
      \frac{t_{\gls{lfsr}, total}}{t_{combination, total}} \approx l \cdot \frac{b \cdot t_{\gls{lfsr}, iter}}{t_{matrix, iter} + (b - 1) t_{\gls{lfsr}, iter}} \approx l \qquad s \gg l \qquad b \gg 1
    \end{equation}

    After combining the equations \eqref{eq:lfsrTime} and \eqref{eq:combinationTime} while once again ignoring irrelevant parts for large $s$ and $b$ we get \eqref{eq:combinationRelationTime}.
    The ideal speedup in this case, ignoring insignificant constant overhead, is $l$ and can therefore not be improved upon as this is defined as the number of cores used.
    We can also conclude that for the best possible speedup $s$ and $b$ should be as large as possible.
    As we generally assume such an implementation to output large $s$, the only practical hurdle is maximizing $b$ which is only limited by the buffer size used.
